# imports a package that allows for running terminal commands
import os


# enables then starts drivers for wifi
os.system('sudo systemctl enable NetworkManager.service && sudo systemctl start NetworkManager.service')


# checks if content with current connection
def checkContent():
        returnValue  = 0
        # asks if the user is content
        while True:
                os.system('nmcli device status')
                content = input('Are you content with these settings? (yes/no) ').strip()

                if content == 'yes' or content == 'y':
                        returnValue = 1
                        break
                elif content == 'no' or content == 'n':
                        returnValue = 0
                        break
                else:
                        print('Please enter a valid response (yes/no)')
        return returnValue


# connects to wifi
def main():
        # check for user content
        isContent = str(checkContent())

        if isContent == '0':
                # lists available networks
                os.system('nmcli device wifi list')


                # user input
                ssid = input('Name of ssid: ')
                pswd = input('Password: ')


                # clears the terminal so your password isnt exposed
                os.system('clear')


                # adds ' to the start and end to make sure that nmcli can run the command
                ssid = "'" + ssid + "'"
                pswd = "'" + pswd + "'"


                # asks for ssid and password then connects
                os.system('nmcli device wifi connect ' + ssid + ' password ' + pswd)
        else:
                return None


main()
